const {resolve} = require('path')
const webpack = require('webpack')
const webpackValidator = require('webpack-validator')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {getIfUtils, removeEmpty} = require('webpack-config-utils')

module.exports = env => {
    const {ifDev, ifProd} = getIfUtils(env)
    const config = webpackValidator({
        entry: {
            app: './app/components/Main.js',
        },
        output: {
            filename: 'bundle.js',
            path: resolve('public'),
        },
        devtool: ifProd('source-map', 'eval'),
        module: {
            loaders: [
                {
                    test: /\.js?$/,
                    loaders: ['babel'],
                    exclude: /node_modules/,
                },
                {
                    test: /\.jsx?$/,
                    loader: 'babel',
                    exclude: /node_modules/,
                    query: {
                        presets: ['react', 'es2015'],
                    }
                },
            ]
        },
        plugins: removeEmpty([
            new HtmlWebpackPlugin({
                template: './app/index.html',
                inject: 'body',
            }),
        ]),
    })

    return config
}
